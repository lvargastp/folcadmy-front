import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl:string = "https://identitytoolkit.googleapis.com/v1/";
  apiKey: "AIzaSyATahZZVpi6uPhMykewS4nsWNiZ478vBnY";

  constructor( private http: HttpClient) { }

  register(user: any): Observable<any>{
    let body = user;
    return this.http.post(this.apiUrl + "accounts:signUp?key=AIzaSyATahZZVpi6uPhMykewS4nsWNiZ478vBnY",body,{

    });
  }

  login(user: any): Observable<any>{
    let body = user;
    return this.http.post(this.apiUrl + "accounts:signInWithPassword?key=AIzaSyATahZZVpi6uPhMykewS4nsWNiZ478vBnY",body,{

    });
  }
}
