import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { SharedModule } from '../shared/shared.module';
import { RoutesComponent } from './container/routes/routes.component';
import { RouterModule } from '@angular/router';
import { LayoutsModule } from '../layouts/layouts.module';



@NgModule({
  declarations: [
    HomeComponent,
    ContactComponent,
    RoutesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    LayoutsModule,
    RouterModule,
  ]
})
export class RoutesModule { }
