import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/modules/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  error: boolean = false;
  user: any;

  registerForm = new FormGroup(
    {
      email: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required)
    }
  )

  constructor(
    private _authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login(){
    this._authService.login(this.registerForm.value).subscribe(
      response =>{
        this.user = response;
        localStorage.setItem('user',JSON.stringify(this.user));
        this.router.navigate(['/home']);
      }, error => {
        console.log(error);
        this.error = true;
      }
    )
  }

}
