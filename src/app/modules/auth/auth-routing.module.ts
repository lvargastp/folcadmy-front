import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Module */
import { AuthModule } from './auth.module';

/* Containers */
import * as authContainer from './container/auth/auth.component'

/* Components */
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

/* Routes */
export const ROUTES: Routes = [
    {
        path: 'login',
        canActivate: [],
        component: LoginComponent,
    },
    {
        path: 'register',
        component: RegisterComponent,
    }
]

@NgModule({
    imports: [AuthModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
